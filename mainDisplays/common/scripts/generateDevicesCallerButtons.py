# -----------------------------------------------------------------------------
# Jython - CSStudio
# -----------------------------------------------------------------------------
# Generates a main display with buttons to call specific devices displays. Read
#   information for the devices from an XML file
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group
# -----------------------------------------------------------------------------
# WP12 - douglas.bezerra.beniz@esss.se
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# Standard libraries
# -----------------------------------------------------------------------------
import xml.etree.ElementTree as ElementTree
import os, sys, time

from time import sleep
from array import array
from jarray import zeros

# -----------------------------------------------------------------------------
# Specific libraries on CS-Studio
# -----------------------------------------------------------------------------
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
from org.csstudio.display.builder.model import WidgetFactory
from org.csstudio.display.builder.model.properties import ActionInfos, OpenDisplayActionInfo

# -----------------------------------------------------------------------------
# Verification if the script is being called from Phoebus or from CS-Studio running on Eclipse
# -----------------------------------------------------------------------------
if 'PHOEBUS' in dir(ScriptUtil):
    from org.phoebus.pv import PVFactory
    from org.phoebus.framework.macros import Macros
else:
    from org.csstudio.display.builder.runtime.pv import PVFactory
    from org.csstudio.display.builder.model.macros import Macros

# -----------------------------------------------------------------------------
# Definition of constants used
# -----------------------------------------------------------------------------
button_width  = 160
button_height = 30
# Dictionary used to set target
display_target = { 'REPLACE':       OpenDisplayActionInfo.Target.REPLACE,
                   'TAB':           OpenDisplayActionInfo.Target.TAB,
                   'WINDOW':        OpenDisplayActionInfo.Target.WINDOW,
                   'STANDALONE':    OpenDisplayActionInfo.Target.STANDALONE }

# -----------------------------------------------------------------------------
# class objects
# -----------------------------------------------------------------------------
logger = ScriptUtil.getLogger()

# -----------------------------------------------------------------------------
# procedures
# -----------------------------------------------------------------------------
def generatorProcedure():
    # Locate XML file relative to the display file
    display_file = ScriptUtil.workspacePathToSysPath(widget.getDisplayModel().getUserData("_input_file"))
    directory    = os.path.dirname(display_file)
    file         = directory + "/scripts/devices.xml"
    display      = widget.getDisplayModel()
    # Components settings
    buttons_by_col  = 14
    x_upCorner      = 30
    y_upCorner      = 85

    # Parse XML
    # Actual content of the XML file would of course depend on what's needed to describe one device.
    xml = ElementTree.parse(file).getroot()
    button_number = 0
    for device in xml.iter("device"):
        device_settings = dict()
        device_macros = Macros()
        for element in device:
            # Debug purposes...
            # logger.info("element.tag: %s" % str(element.tag))
            if str(element.tag) == "macros":
                for macro in element:
                    # Debug purposes...
                    # logger.info("macro.tag: %s; macro.text: %s" % (str(macro.tag), str(macro.text)))
                    device_macros.add(macro.tag, macro.text)
            elif str(element.tag) != "motors":
                device_settings[element.tag] = element.text
        # logger.info("macros: %s" % str(device_macros))
        # Calculating new X and Y coordinates...
        x = x_upCorner
        y = y_upCorner + (button_number * button_height)
        # Creating a new instance of an ActionButton
        instance = createInstance(x, y, device_settings, device_macros)
        # Adding the created ActionButton to the Display
        display.runtimeChildren().addChild(instance)
        # Updating control variables used to position the buttons on screen
        button_number += 1
        if button_number == buttons_by_col:
            # reset count and increase X position (inclue a new column in practice)
            button_number = 0
            x_upCorner += button_width + 20
            y_upCorner = 85
        else:
            y_upCorner += 10


# -----------------------------------------------------------------------------
# Create display:
# -----------------------------------------------------------------------------
# For each 'device', add one action_button display which then links to the correspondent
# .bob display with the macros of the desired device.
def createInstance(x, y, settings, macros):
    # Creating an instance of an openDisplayActionInfo using necessary specific objects, and add it to a list of ActionInfos
    if (settings.get('display')):
        display_file = ('../../../devDisplays/%s/%s' % (settings.get('type'), settings.get('display')))
    else:
        display_file = ('../../../devDisplays/%s/bob/%s.bob' % (settings.get('type'), settings.get('type').split('/')[-1]))
    open_display_action = OpenDisplayActionInfo(settings.get('type'), display_file, macros, display_target.get(settings.get('target')))
    action_infos = ActionInfos([open_display_action])
    # Creating an instance of an ActionButton
    action_button = WidgetFactory.getInstance().getWidgetDescriptor("action_button").createWidget();
    action_button.setPropertyValue("text", settings.get('label'))
    action_button.setPropertyValue("x", x)
    action_button.setPropertyValue("y", y)
    action_button.setPropertyValue("width", button_width)
    action_button.setPropertyValue("height", button_height)
    # ActionInfos list is passed here...
    action_button.setPropertyValue("actions", action_infos)
    return action_button

# -----------------------------------------------------------------------------
# calling the main procedure
# -----------------------------------------------------------------------------
sleep(0.2)              # this was necessary because more than one procedure were being started, probably due to the period of scan of CSStudio thread
generatorProcedure()
