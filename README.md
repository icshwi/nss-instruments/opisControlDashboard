# opisControlDashboard

Control dashboard to centralize OPIs (Operators Interfaces) under commissioning.

# to clone the main repo and all the submodules
git clone --recursive https://gitlab.esss.lu.se/icshwi/nss-instruments/opisControlDashboard.git

Some functions use scripting functions from Phoebus, e.g.:
https://github.com/kasemir/org.csstudio.display.builder/tree/master/org.csstudio.display.builder.runtime/src/org/csstudio/display/builder/runtime/script
